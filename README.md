## INSTRUÇÕES PARA O TESTE TÉCNICO

- Crie um fork deste projeto (https://gitlab.com/Pottencial/tech-test-payment-api/-/forks/new). É preciso estar logado na sua conta Gitlab;
- Adicione @Pottencial (Pottencial Seguradora) como membro do seu fork. Você pode fazer isto em  https://gitlab.com/`your-user`/tech-test-payment-api/settings/members;
 - Quando você começar, faça um commit vazio com a mensagem "Iniciando o teste de tecnologia" e quando terminar, faça o commit com uma mensagem "Finalizado o teste de tecnologia";
 - Commit após cada ciclo de refatoração pelo menos;
 - Não use branches;
 - Você deve prover evidências suficientes de que sua solução está completa indicando, no mínimo, que ela funciona;

## O TESTE
- Construir uma API REST utilizando .Net Core, Java ou NodeJs (com Typescript);
- A API deve expor uma rota com documentação swagger (http://.../api-docs).
- A API deve possuir 3 operações:
  1) Registrar venda: Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento";
  2) Buscar venda: Busca pelo Id da venda;
  3) Atualizar venda: Permite que seja atualizado o status da venda.
     * OBS.: Possíveis status: `Pagamento aprovado` | `Enviado para transportadora` | `Entregue` | `Cancelada`.
- Uma venda contém informação sobre o vendedor que a efetivou, data, identificador do pedido e os itens que foram vendidos;
- O vendedor deve possuir id, cpf, nome, e-mail e telefone;
- A inclusão de uma venda deve possuir pelo menos 1 item;
- A atualização de status deve permitir somente as seguintes transições: 
  - De: `Aguardando pagamento` Para: `Pagamento Aprovado`
  - De: `Aguardando pagamento` Para: `Cancelada`
  - De: `Pagamento Aprovado` Para: `Enviado para Transportadora`
  - De: `Pagamento Aprovado` Para: `Cancelada`
  - De: `Enviado para Transportador`. Para: `Entregue`
- A API não precisa ter mecanismos de autenticação/autorização;
- A aplicação não precisa implementar os mecanismos de persistência em um banco de dados, eles podem ser persistidos "em memória".

## PONTOS QUE SERÃO AVALIADOS
- Arquitetura da aplicação - embora não existam muitos requisitos de negócio, iremos avaliar como o projeto foi estruturada, bem como camadas e suas responsabilidades;
- Programação orientada a objetos;
- Boas práticas e princípios como SOLID, DDD (opcional), DRY, KISS;
- Testes unitários;
- Uso correto do padrão REST;


## Work Executed

### Descreva o que foi feito
Foi desenvolvida uma API, onde é possivel cadastrar vendedores, produtos, pedidos, adicionar produtos ao pedido e cadastrar uma venda que possui um vendedor, um pedido que contem uma lista de produtos por fim controlar o status da venda atraves das atualizações.

Utilizado 
SDK: .NET 6.0
Banco de dados: SQLServer
Docker - Comando utilizado: 
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=Numsey#2022" -p 1450:1433 --name sqlserverdb -d mcr.microsoft.com/mssql/server:2019-latest

**Endpoints**


| Verbo  | Endpoint                   | Parâmetro | Body          |
|--------|----------------------------|-----------|---------------|
| GET    | /Pedido                    | N/A       | N/A           |
| POST   | /Pedido                    | N/A       | Schema Tarefa |
| GET    | /Pedido/{id}               | id        | N/A           |
| PUT    | /Pedido/{id}               | id        | Schema Tarefa |
| DELETE | /Pedido/{id}               | id        | N/A           |
| POST   | /Pedido/AdicionaProduto    | N/A       | Schema Tarefa |
| PUT    | /Pedido/RemoveProduto/{id} | id        | Schema Tarefa |
| GET    | /Produto                   | N/A       | N/A           |
| POST   | /Produto                   | N/A       | Schema Tarefa |
| GET    | /Produto/{id}              | id        | N/A           |
| PUT    | /Produto/{id}              | id        | Schema Tarefa |
| DELETE | /Produto/{id}              | id        | N/A           |
| GET    | /Venda                     | N/A       | N/A           |
| POST   | /Venda                     | N/A       | Schema Tarefa |
| GET    | /Venda/{id}                | id        | N/A           |
| PUT    | /Venda/{id}                | id        | Schema Tarefa |
| DELETE | /Venda/{id}                | id        | N/A           |
| GET    | /Vendedor                  | N/A       | N/A           |
| POST   | /Vendedor                  | N/A       | Schema Tarefa |
| GET    | /Vendedor/{id}             | id        | N/A           |?       | id        | Schema Tarefa |
| DELETE | /Vendedor/{id}             | id        | N/A           |



### Prove que você testou o que fez e que a modificação funcionou
<div align="center">
  <img src="ImgRepo/img01.png">
  <img src="ImgRepo/img02.png">
</div>

### Testes automatizados implementados
<div align="center">
  <img src="ImgRepo/img03.png">
</div>




