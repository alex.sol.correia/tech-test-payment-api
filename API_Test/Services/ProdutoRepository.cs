using System;
using System.Collections.Generic;
using System.Linq;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api_teste.Services
{
    public class ProdutoRepository : IProdutoRepository
    {
        private List<Produto> produtos = new List<Produto>()
        {  
            new Produto
            {
              Preco = 2.99M,
              Nome = "Banana",
              Id = 1
            },
            new Produto
            {
              Preco = 1.20M,
              Nome = "Maça",
              Id = 2
            },
            new Produto
            {
              Preco = 4.00M,
              Nome = "Goiaba",
              Id = 3
            }
        };

        public List<Produto> Produtos 
        { 
            get 
            { 
                return produtos; 
            } 
        }

        public List<Produto> FindAll()
        {
            return this.produtos;
        }

        public Produto Find(int id)
        {
            try
            {
                var produto = this.produtos.FirstOrDefault(p => p.Id == id);

                if (produto == null)
                {
                    throw new Exception($"Produto with Id = {id} not found.");
                }

                return produto;
            }
            catch
            {
                throw new Exception($"Error getting produto with Id = {id}.");
            }
        }

        public bool Create(Produto produto)
        {
            try
            {
                this.produtos.Add(produto);
                var produtoitem = this.produtos.FirstOrDefault(p => p.Id == produto.Id);

                if (produtoitem == null)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

          public bool Update(int id, Produto produto)
        {
            try
            {
                var produtoitem = this.produtos.FirstOrDefault(p => p.Id == id);

                if (produtoitem == null)
                {
                    return false;
                }

                foreach (var item in this.produtos.Where(x => x.Id == produtoitem.Id))
                {
                    item.Preco = produto.Preco;
                    item.Nome = produto.Nome;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Destroy(int id)
        {
            try
            {
                var produtoitem = this.produtos.FirstOrDefault(p => p.Id == id);
                if (produtoitem == null)
                {
                    return false;
                }

                return this.produtos.Remove(produtoitem);
            }
            catch
            {
                return false;
            }
        }
    }
}