﻿using System;
using System.Collections.Generic;
using System.Linq;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api.Models.Enums;

namespace tech_test_payment_api_teste.Services
{
    public class VendaRepository : IVendaRepository
    {
        private List<Venda> vendas = new List<Venda>()
        {
            new Venda
            {
              PedidoId = 3,
              Data = DateTime.Now,
              Status = StatusVenda.Aguardando_pagamento,
              VendedorId = 3,
              Id = 1
            },
            new Venda
            {
              PedidoId = 2,
              Data = DateTime.Now,
              Status = StatusVenda.Aguardando_pagamento,
              VendedorId = 2,
              Id = 2
            },
            new Venda
            {
              PedidoId = 1,
              Data = DateTime.Now,
              Status = StatusVenda.Aguardando_pagamento,
              VendedorId = 1,
              Id = 3
            }
        };

        public List<Venda> Vendas
        {
            get
            {
                return vendas;
            }
        }

        public List<Venda> FindAll()
        {
            return this.vendas;
        }

        public Venda Find(int id)
        {
            try
            {
                var venda = this.vendas.FirstOrDefault(p => p.Id == id);

                if (venda == null)
                {
                    throw new Exception($"Venda with Id = {id} not found.");
                }

                return venda;
            }
            catch
            {
                throw new Exception($"Error getting venda with Id = {id}.");
            }
        }

        public bool Create(Venda venda)
        {
            try
            {
                this.vendas.Add(venda);
                var vendaitem = this.vendas.FirstOrDefault(p => p.Id == venda.Id);

                if (vendaitem == null)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(int id, Venda venda)
        {
            try
            {
                var vendaitem = this.vendas.FirstOrDefault(p => p.Id == id);

                if (vendaitem == null)
                {
                    return false;
                }
        
                foreach (var item in this.vendas.Where(x => x.Id == vendaitem.Id))
                {
                    item.PedidoId = venda.PedidoId;
                    item.Data = venda.Data;
                    item.Status = venda.Status;
                    item.VendedorId = venda.VendedorId;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Destroy(int id)
        {
            try
            {
                var vendaitem = this.vendas.FirstOrDefault(p => p.Id == id);
                if (vendaitem == null)
                {
                    return false;
                }

                return this.vendas.Remove(vendaitem);
            }
            catch
            {
                return false;
            }
        }
    }
}
