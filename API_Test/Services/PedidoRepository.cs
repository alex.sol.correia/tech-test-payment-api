using System;
using System.Collections.Generic;
using System.Linq;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api_teste.Services
{
    public class PedidoRepository : IPedidoRepository
    {
        private List<Pedido> pedidos = new List<Pedido>()
        {  
            new Pedido
            {
              Venda = new Venda(),
              Produtos = new List<Produto>(),
              Codigo = "b4f5a-b4f5a-b4f5a-b4f5a-b4f5a",
              Id = 1
            },
            new Pedido
            {
              Venda = new Venda(),
              Produtos = new List<Produto>(),
              Codigo = "b4f5b-b4f5b-b4f5b-b4f5b-b4f5b",
              Id = 2
            },
            new Pedido
            {
              Venda = new Venda(),
              Produtos = new List<Produto>(),
              Codigo = "b4f5c-b4f5c-b4f5c-b4f5c-b4f5c",
              Id = 3
            }
        };

        public List<Pedido> Pedidos 
        { 
            get 
            { 
                return pedidos; 
            } 
        }

        public List<Pedido> FindAll()
        {
            return this.pedidos;
        }

        public Pedido Find(int id)
        {
            try
            {
                var pedido = this.pedidos.FirstOrDefault(p => p.Id == id);

                if (pedido == null)
                {
                    throw new Exception($"Pedido with Id = {id} not found.");
                }

                return pedido;
            }
            catch
            {
                throw new Exception($"Error getting pedido with Id = {id}.");
            }
        }

        public bool Create(Pedido pedido)
        {
            try
            {
                this.pedidos.Add(pedido);
                var pedidoitem = this.pedidos.FirstOrDefault(p => p.Id == pedido.Id);

                if (pedidoitem == null)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(int id, Pedido pedido)
        {
            try
            {
                var pedidoitem = this.pedidos.FirstOrDefault(p => p.Id == id);

                if (pedidoitem == null)
                {
                    return false;
                }

                foreach (var item in this.pedidos.Where(x => x.Id == pedidoitem.Id))
                {
                    item.Venda = pedido.Venda;
                    item.Produtos = pedido.Produtos;
                    item.Codigo = pedido.Codigo;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Destroy(int id)
        {
            try
            {
                var pedidoitem = this.pedidos.FirstOrDefault(p => p.Id == id);
                if (pedidoitem == null)
                {
                    return false;
                }

                return this.pedidos.Remove(pedidoitem);
            }
            catch
            {
                return false;
            }
        }
    }
}