﻿using System;
using System.Collections.Generic;
using System.Linq;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api_teste.Services
{
    internal class VendedorRepository : IVendedorRepository
    {
        private List<Vendedor> vendedores = new List<Vendedor>()
        {
            new Vendedor
            {
              Telefone = "(11) 98888-8888",
              Email = "exeple@exemple.com",
              Cpf = "999.999.999-99",
              Nome = "João",
              Id = 1
            },
            new Vendedor
            {
              Telefone = "(11) 98888-8888",
              Email = "exeple@exemple.com",
              Cpf = "999.999.999-99",
              Nome = "Bryan",
              Id = 2
            },
            new Vendedor
            {
              Telefone = "(11) 98888-8888",
              Email = "exeple@exemple.com",
              Cpf = "999.999.999-99",
              Nome = "Jonny",
              Id = 3
            }
        };

        public List<Vendedor> Vendedores
        {
            get
            {
                return vendedores;
            }
        }

        public List<Vendedor> FindAll()
        {
            return this.vendedores;
        }

        public Vendedor Find(int id)
        {
            try
            {
                var vendedor = this.vendedores.FirstOrDefault(p => p.Id == id);

                if (vendedor == null)
                {
                    throw new Exception($"Vendedor with Id = {id} not found.");
                }

                return vendedor;
            }
            catch
            {
                throw new Exception($"Error getting vendedor with Id = {id}.");
            }
        }

        public bool Create(Vendedor vendedor)
        {
            try
            {
                this.vendedores.Add(vendedor);
                var vendedoritem = this.vendedores.FirstOrDefault(p => p.Id == vendedor.Id);

                if (vendedoritem == null)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(int id, Vendedor vendedor)
        {
            try
            {
                var vendedoritem = this.vendedores.FirstOrDefault(p => p.Id == id);

                if (vendedoritem == null)
                {
                    return false;
                }

                foreach (var item in this.vendedores.Where(x => x.Id == vendedoritem.Id))
                {
                    item.Telefone = vendedor.Telefone;
                    item.Email = vendedor.Email;
                    item.Cpf = vendedor.Cpf;
                    item.Nome = vendedor.Nome;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Destroy(int id)
        {
            try
            {
                var vendedoritem = this.vendedores.FirstOrDefault(p => p.Id == id);
                if (vendedoritem == null)
                {
                    return false;
                }

                return this.vendedores.Remove(vendedoritem);
            }
            catch
            {
                return false;
            }
        }

    }
}
