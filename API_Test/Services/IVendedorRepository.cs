﻿using System.Collections.Generic;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api_teste.Services
{
    internal interface IVendedorRepository
    {
        public List<Vendedor> FindAll();
    }
}
