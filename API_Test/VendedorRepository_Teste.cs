﻿using System;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api_teste.Services;
using Xunit;

namespace tech_test_payment_api_teste
{
    public class VendedorRepository_Teste
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void TestFindVendedorMock(int id)
        {
            // Arrange
            var repositoryMock = new VendedorRepository();

            //Act
            var vendedor = repositoryMock.Find(id);

            //Assert
            Assert.NotNull(vendedor);
        }

        [Fact]
        public void TestCreateVendedorMock()
        {
            // Arrange
            var vendedor = new Vendedor()
            {
                Telefone = "(11) 98888-8888",
                Email = "rick@exemple.com",
                Cpf = "999.999.999-99",
                Nome = "Rick",
                Id = 4
            };

            var repositoryMock = new VendedorRepository();

            //Act
            var added = repositoryMock.Create(vendedor);

            //Assert
            Assert.True(added);
        }


        [Fact]
        public void TestUpdateVendedorMock()
        {
            // Arrange
            var vendedor = new Vendedor()
            {
                Telefone = "(11) 98888-8888",
                Email = "exemple@exemple.com",
                Cpf = "999.999.999-99",
                Nome = "Nome Atualizado"
            };

            var repositoryMock = new VendedorRepository();

            //Act
            var result = repositoryMock.Update(2, vendedor);

            //Assert
            Assert.True(result);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        public void TestDestroyVendedorMock(int id)
        {
            // Arrange
            var repositoryMock = new VendedorRepository();

            //Act
            var result = repositoryMock.Destroy(id);

            //Assert
            Assert.True(result);
        }


        //Exceções
        [Fact]
        public void TestExcecaoFindVendedor()
        {
            //Arrange
            var repositoryMock = new VendedorRepository();

            //Act     
            //Assert
            Assert.Throws<Exception>(() => repositoryMock.Find(4));

        }
    }
}
