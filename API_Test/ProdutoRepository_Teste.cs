using System;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api_teste.Services;
using Xunit;

namespace tech_test_payment_api_teste
{
    public class ProdutoRepository_Teste
    {
        
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void TestFindProdutoMock(int id)
        {
            // Arrange
            var repositoryMock = new ProdutoRepository();

            //Act
            var produto = repositoryMock.Find(id);

            //Assert
            Assert.NotNull(produto);
        }

        [Fact]
        public void TestCreateProdutoMock()
        {
            // Arrange
            var produto = new Produto()
            {
                Preco = 1.50M,
                Nome = "Uva",
                Id = 4
            };

            var repositoryMock = new ProdutoRepository();

            //Act
            var added = repositoryMock.Create(produto);

            //Assert
            Assert.True(added);
        }


        [Fact]
        public void TestUpdateProdutoMock()
        {
            // Arrange
             var produto = new Produto()
            {
                Preco = 3.50M,
                Nome = "Abacaxi",
            };

            var repositoryMock = new ProdutoRepository();

            //Act
            var result = repositoryMock.Update(2, produto);

            //Assert
            Assert.True(result);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        public void TestDestroyProdutoMock(int id)
        {
            // Arrange
            var repositoryMock = new ProdutoRepository();

            //Act
            var result = repositoryMock.Destroy(id);

            //Assert
            Assert.True(result);
        }


        //Exceções
        [Fact]
        public void TestExcecaoFindProduto()
        {
            //Arrange
            var repositoryMock = new ProdutoRepository();

            //Act     
            //Assert
            Assert.Throws<Exception>(() => repositoryMock.Find(4));

        }
    }
}