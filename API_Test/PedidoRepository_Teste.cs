using System;
using System.Collections.Generic;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api_teste.Services;
using Xunit;

namespace tech_test_payment_api_teste
{
    public class PedidoRepository_Teste
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void TestFindPedidoMock(int id)
        {
            // Arrange
            var repositoryMock = new PedidoRepository();

            //Act
            var pedido = repositoryMock.Find(id);

            //Assert
            Assert.NotNull(pedido);
        }

        [Fact]
        public void TestCreatePedidoMock()
        {
            // Arrange
            var pedido = new Pedido()
            {
                Venda = new Venda(),
                Produtos = new List<Produto>(),
                Codigo = "b4f5d-b4f5d-b4f5d-b4f5d-b4f5d",
                Id = 4
            };

            var repositoryMock = new PedidoRepository();

            //Act
            var added = repositoryMock.Create(pedido);

            //Assert
            Assert.True(added);
        }


        [Fact]
        public void TestUpdatePedidoMock()
        {
            // Arrange
            var pedido = new Pedido()
            {
                Venda = new Venda(),
                Produtos = new List<Produto>(),
                Codigo = "b4f5e-b4f5e-b4f5e-b4f5e-b4f5e",
            };

            var repositoryMock = new PedidoRepository();

            //Act
            var result = repositoryMock.Update(2, pedido);

            //Assert
            Assert.True(result);

        }

        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        public void TestDestroyPedidoMock(int id)
        {
            // Arrange
            var repositoryMock = new PedidoRepository();

            //Act
            var result = repositoryMock.Destroy(id);

            //Assert
            Assert.True(result);
        }


        //Exceções
        [Fact]
        public void TestExcecaoFindPedido()
        {
            //Arrange
            var repositoryMock = new PedidoRepository();

            //Act     
            //Assert
            Assert.Throws<Exception>(() => repositoryMock.Find(4));

        }
    }
}
