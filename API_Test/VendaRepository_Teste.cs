﻿using System;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api.Models.Enums;
using tech_test_payment_api_teste.Services;
using Xunit;

namespace tech_test_payment_api_teste
{
    public class VendaRepository_Teste
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void TestFindVendaMock(int id)
        {
            // Arrange
            var repositoryMock = new VendaRepository();

            //Act
            var venda = repositoryMock.Find(id);

            //Assert
            Assert.NotNull(venda);
        }

        [Fact]
        public void TestCreateVendaMock()
        {
            // Arrange
            var venda = new Venda()
            {
                PedidoId = 3,
                Data = DateTime.Now,
                Status = StatusVenda.Aguardando_pagamento,
                VendedorId = 3,
                Id = 4
            };

            var repositoryMock = new VendaRepository();

            //Act
            var added = repositoryMock.Create(venda);

            //Assert
            Assert.True(added);
        }


        [Fact]
        public void TestUpdateVendaMock()
        {
            // Arrange
            var venda = new Venda()
            {
                PedidoId = 3,
                Status = StatusVenda.Pagamento_aprovado,
                VendedorId = 3
            };

            var repositoryMock = new VendaRepository();

            //Act
            var result = repositoryMock.Update(2, venda);

            //Assert
            Assert.True(result);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        public void TestDestroyVendaMock(int id)
        {
            // Arrange
            var repositoryMock = new VendaRepository();

            //Act
            var result = repositoryMock.Destroy(id);

            //Assert
            Assert.True(result);
        }


        //Exceções
        [Fact]
        public void TestExcecaoFindVenda()
        {
            //Arrange
            var repositoryMock = new VendaRepository();

            //Act     
            //Assert
            Assert.Throws<Exception>(() => repositoryMock.Find(4));

        }
    }
}
