using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models.Dto.VendaDto;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api.Models.Enums;
using tech_test_payment_api.Models.Interfaces;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly IVendaRepository _ivendaRepository;
        private IMapper _mapper;

        public VendaController(IVendaRepository ivendaRepository, IMapper mapper)
        {
            _ivendaRepository = ivendaRepository;
            _mapper = mapper;
        }

        //GET  /Venda
        /// <summary>
        /// Lista os vendas cadastradas
        /// </summary>
        /// <returns>Os itens da lista de vendas</returns>
        /// <response code="200">Retorna os itens da lista de vendas</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IEnumerable<Venda> FindAll()
        {
            return _ivendaRepository.FindAll();
        }


        /// GET  /Venda/{id}
        /// <summary>
        /// Recupera uma venda no banco de dados
        /// </summary>
        /// <returns>Uma venda especifica</returns>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="200">Retorna os itens da lista de vendas</response>
        /// <response code="404">Retorna não encontrar um registro com o id informado</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Find(int id)
        {
           try
            {
                var vendaBanco = _ivendaRepository.Find(id);
                if (vendaBanco != null)
                {
                    ReadVendaDto vendaDto = _mapper.Map<ReadVendaDto>(vendaBanco);
                    return Ok(vendaDto);
                }
            }
            catch (Exception e)
            {
                //TODO: Criar log de error
            }

            return NotFound();
        }

        /// POST /Venda
        /// <summary>
        /// Cria uma nova venda
        /// </summary>
        /// <remarks>
        ///  
        ///     POST /Venda
        ///     {
        ///        "data": "2022-11-03T00:45:45.219Z",
        ///        "vendedorId": 0,
        ///        "pedidoId": 0
        ///     }
        /// 
        /// </remarks>
        /// <param name="vendaDto"></param>
        /// <returns>Nova venda criada</returns>
        /// <response code="201">Retorna o item recém-criado</response>
        /// <response code="400">Se o item for nulo</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Create([FromBody] CreateVendaDto vendaDto)
        {

            Venda venda = _mapper.Map<Venda>(vendaDto);
            venda.Status = StatusVenda.Aguardando_pagamento;

            bool result = _ivendaRepository.Create(venda);

            if (result)
            {
                return CreatedAtAction(nameof(Find), new { Id = venda.Id }, venda);

            }

            return BadRequest(ModelState);

        }

        /// PUT /Venda/{id}
        /// <summary>
        /// Atualiza todos os dados de uma venda específica
        /// </summary>
        /// <remarks>
        ///     A atualização de status deve permitir somente as seguintes transições:
        ///     De: (0) Aguardando pagamento        Para: (1) Pagamento Aprovado
        ///     De: (0) Aguardando pagamento        Para: (4) Cancelada
        ///     De: (1) Pagamento Aprovado          Para: (2) Enviado para Transportadora
        ///     De: (1) Pagamento Aprovado          Para: (4) Cancelada
        ///     De: (2) Enviado para Transportador. Para: (3) Entregue
        ///     
        ///     PUT /Venda/{id}
        ///     {
        ///        "status": 0,
        ///        "data": "2022-11-03T00:45:45.219Z",
        ///        "vendedorId": 0,
        ///        "pedidoId": 0
        ///     }
        /// 
        /// </remarks>
        /// <param name="vendaDto">Entidade que guarda os dados de uma venda que será atualizada</param>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="204">Retorna quando a atualização ocorre com sucesso</response>
        /// <response code="404">Retorna se ocorre algum problema na atualização do registro</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Update(int id, [FromBody] UpdateVendaDto vendaDto)
        {

            Venda venda = _mapper.Map<Venda>(vendaDto);
            bool result =_ivendaRepository.Update(id, venda);

            if (result == true)
            {
                return NoContent();
            }

            return NotFound();
            
        }

        /// DELETE /Venda/{id}
        /// <summary>
        /// Deletar uma venda específica
        /// </summary>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="204">Retorna quando a deleção ocorre com sucesso</response>
        /// <response code="404">Retorna se ocorre algum problema na deleção do registro</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Destroy(int id)
        {
            var result = _ivendaRepository.Destroy(id);

            if (result == false)
            {
                return NotFound();
            }

            return NoContent();
        }

    }
}