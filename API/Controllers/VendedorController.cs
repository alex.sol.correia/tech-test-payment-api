using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models.Dto.VendedorDto;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api.Models.Interfaces;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly IVendedorRepository _ivendedorRepository;
        private IMapper _mapper;

        public VendedorController(IVendedorRepository ivendedorRepository, IMapper mapper)
        {
            _ivendedorRepository = ivendedorRepository;
            _mapper = mapper;
        }

        //GET  /vendedor
        /// <summary>
        /// Lista os vendedores cadastrados
        /// </summary>
        /// <returns>Os itens da lista de vendedores</returns>
        /// <response code="200">Retorna os itens da lista de vendedores</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IEnumerable<Vendedor> FindAll()
        {
            return _ivendedorRepository.FindAll();
        }

        /// GET  /vendedor/{id}
        /// <summary>
        /// Recupera um vendedor no banco de dados
        /// </summary>
        /// <returns>Um vendedor especifico</returns>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="200">Retorna os itens da lista de vendedores</response>
        /// <response code="404">Retorna não encontrar um registro com o id informado</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Find(int id)
        {
            try
            {
                var vendedorBanco = _ivendedorRepository.Find(id);
                if (vendedorBanco != null)
                {
                    ReadVendedorDto vendedorDto = _mapper.Map<ReadVendedorDto>(vendedorBanco);
                    return Ok(vendedorDto);
                }
            }
            catch (Exception e)
            {
                //TODO: Criar log de error
            }

            return NotFound();
        }

        /// POST /vendedor
        /// <summary>
        /// Cria um novo vendedor
        /// </summary>
        /// <remarks>
        ///  
        ///     POST /vendedores
        ///     {
        ///        "nome": "Exemple",
        ///        "cpf": "999.999.999-88",
        ///        "email": "exemple@exemple.com",
        ///        "telefone": "(99) 98888-7777)"
        ///     }
        /// 
        /// </remarks>
        /// <param name="vendedorDto"></param>
        /// <returns>Novo vendedor criado</returns>
        /// <response code="201">Retorna o item recém-criado</response>
        /// <response code="400">Se o item for nulo</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Create([FromBody] CreateVendedorDto vendedorDto)
        {

            Vendedor vendedor = _mapper.Map<Vendedor>(vendedorDto);

            bool result = _ivendedorRepository.Create(vendedor);

            if (result)
            {
                return CreatedAtAction(nameof(Find), new { Id = vendedor.Id }, vendedor);

            }

            return BadRequest(ModelState);

        }

        /// PUT /vendedor/{id}
        /// <summary>
        /// Atualiza todos os dados de um vendedor específico
        /// </summary>
        /// <remarks>
        ///  
        ///     PUT /vendedor/{id}
        ///     {
        ///        "nome": "Exemple",
        ///        "cpf": "999.999.999-88",
        ///        "email": "exemple@exemple.com",
        ///        "telefone": "(99) 98888-7777)"
        ///     }
        /// 
        /// </remarks>
        /// <param name="vendedorDto">Entidade que guarda os dados do vendedor para atualizar no banco</param>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="204">Retorna quando a atualização ocorre com sucesso</response>
        /// <response code="404">Retorna se ocorre algum problema na atualização do registro</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Update(int id, [FromBody] UpdateVendedorDto vendedorDto)
        {

            Vendedor vendedor = _mapper.Map<Vendedor>(vendedorDto);

            bool result =_ivendedorRepository.Update(id, vendedor);

            if (result == true)
            {
                return NoContent();
            }

            return NotFound();
            
        }

        /// DELETE /vendedor/{id}
        /// <summary>
        /// Deletar um vendedor específico
        /// </summary>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="204">Retorna quando a deleção ocorre com sucesso</response>
        /// <response code="404">Retorna se ocorre algum problema na deleção do registro</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Destroy(int id)
        {
            var result = _ivendedorRepository.Destroy(id);

            if (result == false)
            {
                return NotFound();
            }

            return NoContent();
        }

    }
}