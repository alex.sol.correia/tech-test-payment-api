using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models.Dto.PedidoDto;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api.Models.Interfaces;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PedidoController : ControllerBase
    {
         private readonly IPedidoRepository _ipedidoRepository;
        private IMapper _mapper;

        public PedidoController(IPedidoRepository ipedidoRepository, IMapper mapper)
        {
            _ipedidoRepository = ipedidoRepository;
            _mapper = mapper;
        }

        //GET  /Pedido
        /// <summary>
        /// Lista os pedidos cadastrados
        /// </summary>
        /// <returns>Os itens da lista de pedidos</returns>
        /// <response code="200">Retorna os itens da lista de pedidos</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IEnumerable<Pedido> FindAll()
        {
            return _ipedidoRepository.FindAll();
        }


        /// GET  /Pedido/{id}
        /// <summary>
        /// Recupera um pedido no banco de dados
        /// </summary>
        /// <returns>Uma pedido especifico</returns>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="200">Retorna os itens da lista de pedidos</response>
        /// <response code="404">Retorna não encontrar um registro com o id informado</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Find(int id)
        {
           try
            {
                var pedidoBanco = _ipedidoRepository.Find(id);
                if (pedidoBanco != null)
                {
                    ReadPedidoDto pedidoDto = _mapper.Map<ReadPedidoDto>(pedidoBanco);
                    return Ok(pedidoDto);
                }
            }
            catch (Exception e)
            {
                //TODO: Criar log de error
            }

            return NotFound();
        }

        /// POST /Pedido
        /// <summary>
        /// Cria uma novo pedido
        /// </summary>
        /// <remarks>
        ///  
        ///     POST /Pedido
        ///     {
        ///       "codigo": "Exemple"
        ///     }
        /// 
        /// </remarks>
        /// <param name="pedidoDto"></param>
        /// <returns>Novo pedido criado</returns>
        /// <response code="201">Retorna o item recém-criado</response>
        /// <response code="400">Se o item for nulo</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Create([FromBody] CreatePedidoDto pedidoDto)
        {

            Pedido pedido = _mapper.Map<Pedido>(pedidoDto);
            pedido.Codigo = pedidoDto.Codigo;

            bool result = _ipedidoRepository.Create(pedido);

            if (result)
            {
                return CreatedAtAction(nameof(Find), new { Id = pedido.Id }, pedido);

            }

            return BadRequest(ModelState);

        }


        /// POST /Pedido/AdicionaProduto
        /// <summary>
        /// Cria a relação de um produto com um pedido
        /// </summary>
        /// <remarks>
        ///  
        ///     POST /Pedido/AdicionaProduto
        ///     {
        ///       "pedidoId": 0,
        ///       "produtoId": 0
        ///     }
        /// 
        /// </remarks>
        /// <param name="addPedidoProduto"></param>
        /// <returns>Novo pedidoProduto criado</returns>
        /// <response code="201">Retorna o item recém-criado</response>
        /// <response code="400">Se o item for nulo</response>
        [HttpPost("/Pedido/AdicionaProduto")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult AddPedidoProduto([FromBody] AddPedidoProduto addPedidoProduto)
        {
            bool result = _ipedidoRepository.AddPedidoProduto(addPedidoProduto.PedidoId, addPedidoProduto.ProdutoId);

            if (result)
            {
                return CreatedAtAction(nameof(Find), new { Id = addPedidoProduto.PedidoId }, addPedidoProduto);
            }

            return BadRequest(ModelState);

        }

        /// PUT /Pedido/{id}
        /// <summary>
        /// Atualiza todos os dados de um pedido específica
        /// </summary>
        /// <remarks>
        ///  
        ///     PUT /Pedido/{id}
        ///     {
        ///        "codigo": "Exemple"
        ///     }
        /// 
        /// </remarks>
        /// <param name="pedidoDto">Entidade que guarda os dados de um pedido que será atualizada</param>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="204">Retorna quando a atualização ocorre com sucesso</response>
        /// <response code="404">Retorna se ocorre algum problema na atualização do registro</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Update(int id, [FromBody] UpdatePedidoDto pedidoDto)
        {

            Pedido pedido = _mapper.Map<Pedido>(pedidoDto);

            bool result =_ipedidoRepository.Update(id, pedido);

            if (result == true)
            {
                return NoContent();
            }

            return NotFound();
            
        }

        /// PUT /Pedido/RemoveProduto/{id}
        /// <summary>
        /// Remove um produto da relação pedidoProduto
        /// </summary>
        /// <remarks>
        ///  
        ///     PUT /Pedido/RemoveProduto/{id}
        ///     {
        ///        "produtoId": 0
        ///     }
        /// 
        /// </remarks>
        /// <param name="removePedidoProduto">Entidade que guarda os dados de um pedidoProduto que será atualizada</param>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="204">Retorna quando a atualização ocorre com sucesso</response>
        /// <response code="404">Retorna se ocorre algum problema na atualização do registro</response>
        [HttpPut("/Pedido/RemoveProduto/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult RemovePedidoProduto(int id, [FromBody] RemovePedidoProduto removePedidoProduto)
        {

            bool result = _ipedidoRepository.RemovePedidoProduto(id, removePedidoProduto.ProdutoId);

            if (result == true)
            {
                return NoContent();
            }

            return NotFound();
            
        }

        /// DELETE /Pedido/{id}
        /// <summary>
        /// Deletar um pedido específica
        /// </summary>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="204">Retorna quando a deleção ocorre com sucesso</response>
        /// <response code="404">Retorna se ocorre algum problema na deleção do registro</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Destroy(int id)
        {
            var result = _ipedidoRepository.Destroy(id);

            if (result == false)
            {
                return NotFound();
            }

            return NoContent();
        }

    }
}