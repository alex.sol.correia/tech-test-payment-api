using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models.Dto.ProdutoDto;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api.Models.Interfaces;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly IProdutoRepository _iprodutoRepository;
        private IMapper _mapper;

        public ProdutoController(IProdutoRepository iprodutoRepository, IMapper mapper)
        {
            _iprodutoRepository = iprodutoRepository;
            _mapper = mapper;
        }

        //GET  /Produto
        /// <summary>
        /// Lista os produtos cadastrados
        /// </summary>
        /// <returns>Os itens da lista de produtos</returns>
        /// <response code="200">Retorna os itens da lista de produtos</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IEnumerable<Produto> FindAll()
        {
            return _iprodutoRepository.FindAll();
        }

        /// GET  /Produto/{id}
        /// <summary>
        /// Recupera um produto no banco de dados
        /// </summary>
        /// <returns>Um produto especifico</returns>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="200">Retorna os itens da lista de produtos</response>
        /// <response code="404">Retorna não encontrar um registro com o id informado</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Find(int id)
        {
            try
            {
                var produtoBanco = _iprodutoRepository.Find(id);
                if (produtoBanco != null)
                {
                    ReadProdutoDto produtoDto = _mapper.Map<ReadProdutoDto>(produtoBanco);
                    return Ok(produtoDto);
                }
            }
            catch (Exception e)
            {
                //TODO: Criar log de error
            }

            return NotFound();
        }

        /// POST /Produto
        /// <summary>
        /// Cria um novo produto
        /// </summary>
        /// <remarks>
        ///  
        ///     POST /Produto
        ///     {
        ///        "nome": "Exemple",
        ///        "preco": 0
        ///     }
        /// 
        /// </remarks>
        /// <param name="produtoDto"></param>
        /// <returns>Novo produto criado</returns>
        /// <response code="201">Retorna o item recém-criado</response>
        /// <response code="400">Se o item for nulo</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Create([FromBody] CreateProdutoDto produtoDto)
        {

            Produto produto = _mapper.Map<Produto>(produtoDto);

            bool result = _iprodutoRepository.Create(produto);

            if (result)
            {
                return CreatedAtAction(nameof(Find), new { Id = produto.Id }, produto);

            }

            return BadRequest(ModelState);

        }

        /// PUT /Produto/{id}
        /// <summary>
        /// Atualiza todos os dados de um produto específico
        /// </summary>
        /// <remarks>
        ///  
        ///     PUT /Produto/{id}
        ///     {
        ///        "nome": "Exemple",
        ///        "preco": 0
        ///     }
        /// 
        /// </remarks>
        /// <param name="produtoDto">Entidade que guarda os dados do produto para atualizar no banco</param>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="204">Retorna quando a atualização ocorre com sucesso</response>
        /// <response code="404">Retorna se ocorre algum problema na atualização do registro</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Update(int id, [FromBody] UpdateProdutoDto produtoDto)
        {

            Produto produto = _mapper.Map<Produto>(produtoDto);

            bool result =_iprodutoRepository.Update(id, produto);

            if (result == true)
            {
                return NoContent();
            }

            return NotFound();
            
        }

        /// DELETE /Produto/{id}
        /// <summary>
        /// Deletar um produto específico
        /// </summary>
        /// <param name="id">id de um registro existente no banco de dados</param>
        /// <response code="204">Retorna quando a deleção ocorre com sucesso</response>
        /// <response code="404">Retorna se ocorre algum problema na deleção do registro</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Destroy(int id)
        {
            var result = _iprodutoRepository.Destroy(id);

            if (result == false)
            {
                return NotFound();
            }

            return NoContent();
        }

    
    }
}