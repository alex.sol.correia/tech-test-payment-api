using AutoMapper;
using tech_test_payment_api.Models.Dto.VendedorDto;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Models.Profiles
{
    public class VendedorProfiles : Profile
    {
        public VendedorProfiles()
        {
            CreateMap<CreateVendedorDto, Vendedor>();
            CreateMap<Vendedor, ReadVendedorDto>();
            CreateMap<UpdateVendedorDto, Vendedor>();
        }

    }
}