using AutoMapper;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api.Models.Dto.VendaDto;

namespace tech_test_payment_api.Models.Profiles
{
    public class VendaProfiles : Profile
    {
        public VendaProfiles()
        {
            CreateMap<CreateVendaDto, Venda>();
            CreateMap<Venda, ReadVendaDto>();
            CreateMap<UpdateVendaDto, Venda>();
        }
    }
}