using AutoMapper;
using tech_test_payment_api.Models.Dto.ProdutoDto;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Models.Profiles
{
    public class ProdutoProfiles : Profile
    {
        public ProdutoProfiles()
        {
            CreateMap<CreateProdutoDto, Produto>();
            CreateMap<Produto, ReadProdutoDto>();
            CreateMap<UpdateProdutoDto, Produto>();
        }
      
    }
}