using AutoMapper;
using tech_test_payment_api.Models.Dto.PedidoDto;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Models.Profiles
{
    public class PedidoProfiles : Profile
    {
        public PedidoProfiles()
        {
            CreateMap<CreatePedidoDto, Pedido>();
            CreateMap<Pedido, ReadPedidoDto>();
            CreateMap<UpdatePedidoDto, Pedido>();
        }
    }
}