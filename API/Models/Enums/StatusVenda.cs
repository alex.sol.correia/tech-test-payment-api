namespace tech_test_payment_api.Models.Enums
{
    public enum StatusVenda
    {
        Aguardando_pagamento,
        Pagamento_aprovado,	
        Enviado_para_transportadora,
        Entregue,
        Cancelada
    }
}