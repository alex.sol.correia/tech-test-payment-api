using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Models.Interfaces
{
    public interface IVendaRepository
    {
        public List<Venda> FindAll();
        public Venda Find(int id);
        public bool Create(Venda venda);
        public bool Update(int id, Venda venda);
        public bool Destroy(int id);
    }
}