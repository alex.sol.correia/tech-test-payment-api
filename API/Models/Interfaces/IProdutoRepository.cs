using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Models.Interfaces
{
    public interface IProdutoRepository
    {
        public List<Produto> FindAll();
        public Produto Find(int id);
        public bool Create(Produto produto);
        public bool Update(int id, Produto produto);
        public bool Destroy(int id);
    }
}