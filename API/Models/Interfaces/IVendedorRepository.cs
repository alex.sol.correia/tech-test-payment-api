using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Models.Interfaces
{
    public interface IVendedorRepository
    {
        public List<Vendedor> FindAll();
        public Vendedor Find(int id);
        public bool Create(Vendedor vendedor);
        public bool Update(int id, Vendedor vendedor);
        public bool Destroy(int id);
        
    }
}