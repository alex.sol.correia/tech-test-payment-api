using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Models.Interfaces
{
    public interface IPedidoRepository
    {
        public List<Pedido> FindAll();
        public Pedido Find(int id);
        public bool Create(Pedido pedido);
        public bool Update(int id, Pedido pedido);
        public bool Destroy(int id);
         public bool AddPedidoProduto(int pedidoId, int produtoId);
         public bool RemovePedidoProduto(int pedidoId, int produtoId);
    }
}