using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Models.Context
{
    public class AppDbContext : DbContext
    {

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options){}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Vendedor>()
                .HasOne(vendedor => vendedor.Venda)
                .WithOne(venda => venda.Vendedor)
                .HasForeignKey<Venda>(venda => venda.VendedorId);

            builder.Entity<Pedido>()
                .HasOne(pedido => pedido.Venda)
                .WithOne(venda => venda.Pedido)
                .HasForeignKey<Venda>(venda => venda.PedidoId);

        }

        public DbSet<Vendedor> Vendedores 
        { 
            get; 
            set; 
        }

        public DbSet<Produto> Produtos 
        { 
            get; 
            set; 
        }

        public DbSet<Venda> Vendas 
        { 
            get; 
            set; 
        }

        public DbSet<Pedido> Pedidos 
        { 
            get;
            set; 
        }
        
    }
}