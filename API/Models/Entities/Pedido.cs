using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Models.Entities
{
    public class Pedido
    {
        [Key]
        [Required]
        public int Id 
        { 
            get; 
            set; 
        }

        public string Codigo 
        { 
            get; 
            set; 
        }

        public virtual List<Produto> Produtos 
        { 
            get; 
            set; 
        }

        [JsonIgnore]
        public virtual Venda Venda 
        { 
            get; 
            set; 
        }           
        
    }
}