using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Models.Entities
{
    public class Vendedor
    {
        [Key]
        [Required]
        public int Id 
        { 
            get; 
            set; 
        }

        [Required(ErrorMessage = "O campo de nome é obrigatório")]
        public string Nome 
        { 
            get; 
            set; 
        }

        [Required(ErrorMessage = "O campo de cpf é obrigatório")]
        public string Cpf 
        { 
            get; 
            set; 
        }

        [Required(ErrorMessage = "O campo de e-mail é obrigatório")]
        public string Email 
        { 
            get; 
            set; 
        }

        public string Telefone 
        { 
            get; 
            set; 
        }    

        [JsonIgnore]
        public virtual Venda Venda 
        { 
            get; 
            set; 
        }   
        
    }
}