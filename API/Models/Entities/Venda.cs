using System.ComponentModel.DataAnnotations;
using tech_test_payment_api.Models.Enums;

namespace tech_test_payment_api.Models.Entities
{
    public class Venda
    {
        [Key]
        [Required]
        public int Id 
        { 
            get; 
            set; 
        }

        public virtual Vendedor Vendedor 
        { 
            get; 
            set; 
        }

        public int VendedorId 
        { 
            get; 
            set; 
        }

        public StatusVenda Status 
        { 
            get; 
            set; 
        } 

        public DateTime Data 
        { 
            get; 
            set; 
        }

        public virtual Pedido Pedido 
        { 
            get; 
            set; 
        }

        public int PedidoId 
        { 
            get; 
            set; 
        } 
        
    }
}
