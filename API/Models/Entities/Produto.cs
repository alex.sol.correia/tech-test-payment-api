using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace tech_test_payment_api.Models.Entities
{
    public class Produto
    {
        [Key]
        [Required]
        public int Id 
        { 
            get; 
            set; 
        }

        public string Nome 
        { 
            get; 
            set; 
        }

        public Decimal Preco 
        { 
            get; 
            set; 
        }

        [JsonIgnore]
        public virtual List<Pedido> Pedidos 
        { 
            get; 
            set; 
        } 
            
    }
}