using tech_test_payment_api.Models.Context;
using tech_test_payment_api.Models.Interfaces;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Models.Repositories
{
    public class VendaRepository : IVendaRepository
    {
        private readonly AppDbContext _context;
        public VendaRepository(AppDbContext context)
        {
            _context = context;
        }

        public List<Venda> FindAll()
        {
            return _context.Vendas.ToList();
        }

        public Venda Find(int id)
        {
            try
            {
                var venda = _context.Vendas.FirstOrDefault(p => p.Id == id);

                if (venda == null)
                {
                    throw new Exception($"Venda com Id = {id} não encontrado.");
                }

                return venda;
            }
            catch
            {
                throw new Exception($"Error ao pegar venda com o Id = {id}.");
            }
        }


        public bool Create(Venda venda)
        {
            try
            {
                _context.Vendas.Add(venda);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(int id, Venda venda)
        {
            try
            {
                var vendaBanco = _context.Vendas.FirstOrDefault(p => p.Id == id);
                if (vendaBanco == null)
                {
                    return false;
                }

                if(StatusValido((int)vendaBanco.Status, (int)venda.Status) == false)
                {
                    return false;
                }      
                    
                vendaBanco.VendedorId = venda.VendedorId;
                vendaBanco.Status = venda.Status;
                vendaBanco.Data = venda.Data;

                _context.SaveChanges();

                return true;

            }
            catch
            {
                return false;
            }

        }

        public bool StatusValido(int statusBanco, int status)
        {
            switch (statusBanco)
            {
                case 0:                   
                    if(status != 1 && status != 4)
                    {
                        return false;
                    }
                    break;

                case 1:
                    if(status != 2 && status != 4)
                    {
                        return false;
                    }
                    break;

                case 2:
                    if(status != 3)
                    {
                        return false;
                    }
                    break;

                default:
                    break;
            }

            return true;
        }

        public bool Destroy(int id)
        {
            try
            {
                var vendaBanco = _context.Vendas.FirstOrDefault(p => p.Id == id);

                if (vendaBanco == null)
                {
                    return false;
                }

                _context.Vendas.Remove(vendaBanco);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}