using tech_test_payment_api.Models.Context;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api.Models.Interfaces;

namespace tech_test_payment_api.Models.Repositories
{
    public class VendedorRepository : IVendedorRepository
    {
        private readonly AppDbContext _context;
        public VendedorRepository(AppDbContext context)
        {
            _context = context;
        }

        public List<Vendedor> FindAll()
        {
            return _context.Vendedores.ToList();
        }

        public Vendedor Find(int id)
        {
            try
            {
                var vendedor = _context.Vendedores.FirstOrDefault(p => p.Id == id);
                if (vendedor == null)
                {
                    throw new Exception($"Vendedor com Id = {id} não encontrado.");
                }

                return vendedor;
            }
            catch
            {
                throw new Exception($"Error ao pegar vendedor com o Id = {id}.");
            }
        }


        public bool Create(Vendedor vendedor)
        {
            try
            {
                _context.Vendedores.Add(vendedor);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(int id, Vendedor vendedor)
        {
            try
            {
                var vendedorBanco = _context.Vendedores.FirstOrDefault(p => p.Id == id);

                if (vendedorBanco == null)
                {
                    return false;
                }

                vendedorBanco.Nome = vendedor.Nome;
                vendedorBanco.Cpf = vendedor.Cpf;
                vendedorBanco.Email = vendedor.Email;
                vendedorBanco.Telefone = vendedor.Telefone;

                _context.SaveChanges();

                return true;

            }
            catch
            {
                return false;
            }

        }

        public bool Destroy(int id)
        {
            try
            {
                var vendedorBanco = _context.Vendedores.FirstOrDefault(p => p.Id == id);

                if (vendedorBanco == null)
                {
                    return false;
                }

                _context.Vendedores.Remove(vendedorBanco);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}