using tech_test_payment_api.Models.Context;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api.Models.Interfaces;

namespace tech_test_payment_api.Models.Repositories
{
    public class ProdutoRepository : IProdutoRepository
    {
        private readonly AppDbContext _context;
        public ProdutoRepository(AppDbContext context)
        {
            _context = context;
        }

        public List<Produto> FindAll()
        {
            return _context.Produtos.ToList();
        }

        public Produto Find(int id)
        {
            try
            {
                var produto = _context.Produtos.FirstOrDefault(p => p.Id == id);
                if (produto == null)
                {
                    throw new Exception($"Produto com Id = {id} não encontrado.");
                }

                return produto;
            }
            catch
            {
                throw new Exception($"Error ao pegar produto com o Id = {id}.");
            }
        }


        public bool Create(Produto produto)
        {
            try
            {
                _context.Produtos.Add(produto);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(int id, Produto produto)
        {
            try
            {
                var produtoBanco = _context.Produtos.FirstOrDefault(p => p.Id == id);

                if (produtoBanco == null)
                {
                    return false;
                }

                produtoBanco.Nome = produto.Nome;
                produtoBanco.Preco = produto.Preco;

                _context.SaveChanges();

                return true;

            }
            catch
            {
                return false;
            }

        }

        public bool Destroy(int id)
        {
            try
            {
                var produtoBanco = _context.Produtos.FirstOrDefault(p => p.Id == id);

                if (produtoBanco == null)
                {
                    return false;
                }

                _context.Produtos.Remove(produtoBanco);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}