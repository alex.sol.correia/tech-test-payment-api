using tech_test_payment_api.Models.Context;
using tech_test_payment_api.Models.Entities;
using tech_test_payment_api.Models.Interfaces;

namespace tech_test_payment_api.Models.Repositories
{
    public class PedidoRepository : IPedidoRepository
    {
        private readonly AppDbContext _context;
        public PedidoRepository(AppDbContext context)
        {
            _context = context;
        }

        public List<Pedido> FindAll()
        {
            return _context.Pedidos.ToList();
        }

        public Pedido Find(int id)
        {
            try
            {
                var pedido = _context.Pedidos.FirstOrDefault(p => p.Id == id);
                if (pedido == null)
                {
                    throw new Exception($"Pedido com Id = {id} não encontrado.");
                }

                return pedido;
            }
            catch
            {
                throw new Exception($"Error ao pegar pedido com o Id = {id}.");
            }
        }


        public bool Create(Pedido pedido)
        {
            try
            {
                _context.Pedidos.Add(pedido);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool AddPedidoProduto(int pedidoId, int produtoId)
        {
            try
            {
                var produtoBanco = _context.Produtos.FirstOrDefault(p => p.Id == produtoId);
                var pedidoBanco = _context.Pedidos.FirstOrDefault(p => p.Id == pedidoId);

                pedidoBanco.Produtos.Add(produtoBanco);
            
                _context.SaveChanges();
      
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool RemovePedidoProduto(int pedidoId, int produtoId)
        {
            try
            {
                var produtoBanco = _context.Produtos.FirstOrDefault(p => p.Id == produtoId);
                var pedidoBanco = _context.Pedidos.FirstOrDefault(p => p.Id == pedidoId);

                pedidoBanco.Produtos.Remove(produtoBanco);
            
                _context.SaveChanges();
      
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(int id, Pedido pedido)
        {
            try
            {
                var pedidoBanco = _context.Pedidos.FirstOrDefault(p => p.Id == id);

                if (pedidoBanco == null)
                {
                    return false;
                }

                pedidoBanco.Codigo = pedido.Codigo;
                _context.SaveChanges();

                return true;

            }
            catch
            {
                return false;
            }

        }

        public bool Destroy(int id)
        {
            try
            {
                var pedidoBanco = _context.Pedidos.FirstOrDefault(p => p.Id == id);

                if (pedidoBanco == null)
                {
                    return false;
                }

                _context.Pedidos.Remove(pedidoBanco);
                _context.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}