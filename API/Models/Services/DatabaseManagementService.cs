using tech_test_payment_api.Models.Context;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Models.Services
{
    public static class DatabaseManagementService
    {
        public static void MigrationInitialisation(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var serviceDb = serviceScope.ServiceProvider.GetService<AppDbContext>();
                serviceDb.Database.Migrate();
            }
            
        }
    }
}