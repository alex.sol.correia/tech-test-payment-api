

namespace tech_test_payment_api.Models.Dto.ProdutoDto
{
    public class CreateProdutoDto
    {
        public string Nome 
        { 
            get; 
            set; 
        }

        public Decimal Preco 
        { 
            get; 
            set; 
        }
        
    }
}