using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models.Dto.ProdutoDto
{
    public class ReadProdutoDto
    {
        [Key]
        [Required]
        public int Id 
        { 
            get;
            set; 
        }

        public string Nome 
        { 
            get; 
            set; 
        }

        public Decimal Preco 
        { 
            get; 
            set; 
        }
        
    }
}