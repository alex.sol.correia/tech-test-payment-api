namespace tech_test_payment_api.Models.Dto.PedidoDto
{
    public class AddPedidoProduto
    {
        public int PedidoId 
        { 
            get; 
            set; 
        }
        public int ProdutoId 
        { 
            get; 
            set; 
        }    
           
    }
}