using System.ComponentModel.DataAnnotations;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Models.Dto.PedidoDto
{
    public class ReadPedidoDto
    {
        [Key]
        [Required]
        public int Id 
        { 
            get; 
            set; 
        }

        public string Codigo 
        { 
            get; 
            set; 
        }

        public List<Produto> Produtos 
        { 
            get; 
            set; 
        }
        
    }
}