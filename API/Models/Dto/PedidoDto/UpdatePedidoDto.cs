using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models.Dto.PedidoDto
{
    public class UpdatePedidoDto
    {
        [Key]
        [Required]
        public int Id 
        { 
            get; 
            set; 
        }

        public string Codigo 
        { 
            get; 
            set; 
        } 
        
    }
}