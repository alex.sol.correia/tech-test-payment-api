using System.ComponentModel.DataAnnotations;
using tech_test_payment_api.Models.Entities;

namespace tech_test_payment_api.Models.Dto.VendaDto
{
    public class CreateVendaDto
    {
        public DateTime Data 
        { 
            get; 
            set;
        } 

        public int VendedorId 
        { 
            get; 
            set; 
        }

        public int PedidoId 
        { 
            get; 
            set; 
        } 
        
    }
}